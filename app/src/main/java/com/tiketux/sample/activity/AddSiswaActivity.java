package com.tiketux.sample.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tiketux.sample.R;
import com.tiketux.sample.network.RetrofitClient;
import com.tiketux.sample.response.TambahSiswaResponse;
import com.tiketux.sample.util.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mochamad Taufik on 17-Jun-19.
 * Email   : thidayat13@gmail.com
 */

public class AddSiswaActivity extends AppCompatActivity {

    private EditText etNpm, etNama, etJurusan, etNoTelp, etAlamat, etKodePos;
    private Button btnSimpan;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_siswa);

        etNpm           = findViewById(R.id.et_npm);
        etNama          = findViewById(R.id.et_nama);
        etJurusan       = findViewById(R.id.et_jurusan);
        etNoTelp        = findViewById(R.id.et_notelp);
        etAlamat        = findViewById(R.id.et_alamat);
        etKodePos       = findViewById(R.id.et_kodepos);
        btnSimpan       = findViewById(R.id.btn_simpan);

        progressDialog = new ProgressDialog(this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validasiForm();
            }
        });
    }

    private void validasiForm() {
        if(etNpm.getText().toString().isEmpty() || etNama.getText().toString().isEmpty()
                || etJurusan.getText().toString().isEmpty() || etNoTelp.getText().toString().isEmpty()
                || etAlamat.getText().toString().isEmpty() || etKodePos.getText().toString().isEmpty()){
            Utils.longToast(this, getString(R.string.no_data));

        } else {
            if(Utils.isNetworkOn(this)){
                postForm();
            } else {
                Utils.longToast(this, getString(R.string.no_connection));
            }
        }
    }

    private void postForm() {

        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Call<TambahSiswaResponse> call = RetrofitClient.provideRetrofit().inputSiswa(
                etNpm.getText().toString(),
                etNama.getText().toString(),
                etJurusan.getText().toString(),
                etNoTelp.getText().toString(),
                etAlamat.getText().toString(),
                etKodePos.getText().toString());

        call.enqueue(new Callback<TambahSiswaResponse>() {
            @Override
            public void onResponse(Call<TambahSiswaResponse> call, Response<TambahSiswaResponse> response) {
                progressDialog.dismiss();

                if (response.body() != null) {
                    TambahSiswaResponse response1 = response.body();
                    if(response1.getStatus().equalsIgnoreCase("OK")){

                        Utils.longToast(getApplicationContext(), response1.getMessage());
                        Intent returnIntent =  new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();

                    }else{
                        Utils.longToast(getApplicationContext(), response1.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<TambahSiswaResponse> call, Throwable t) {
                progressDialog.dismiss();
                Utils.longToast(getApplicationContext(), t.getLocalizedMessage());
            }
        });
    }
}
