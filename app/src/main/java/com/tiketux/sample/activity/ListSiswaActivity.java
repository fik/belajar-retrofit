package com.tiketux.sample.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.tiketux.sample.R;
import com.tiketux.sample.adapter.SiswaAdapter;
import com.tiketux.sample.model.Siswa;
import com.tiketux.sample.network.RetrofitClient;
import com.tiketux.sample.response.DeleteSiswaResponse;
import com.tiketux.sample.response.SiswaResponse;
import com.tiketux.sample.util.DividerItemDecorationPadding;
import com.tiketux.sample.util.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mochamad Taufik on 13-Jun-19.
 * Email   : thidayat13@gmail.com
 */

public class ListSiswaActivity extends AppCompatActivity {

    //inisialisasi widget
    private ProgressBar progressBar;
    private RecyclerView rvSiswa;
    private FloatingActionButton fab;

    //inisialisasi list
    private List<Siswa> listSiswa = new ArrayList<>();

    //inisisalisasi adapter
    private SiswaAdapter siswaAdapter;

    private static final int INTENT_TAMBAH = 21;
    private static final int INTENT_EDIT = 22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar     = findViewById(R.id.progress);
        rvSiswa         = findViewById(R.id.rv_siswa);
        fab             = findViewById(R.id.fab);

        //setRecycler
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL, false);
        rvSiswa.setLayoutManager(layoutManager);
        rvSiswa.addItemDecoration(new DividerItemDecorationPadding(getApplicationContext(), R.color.divider));

        //inisisalisasi adapter
        siswaAdapter = new SiswaAdapter(this);

        //get siswa
        getSiswa();
        onClickAdapter();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ListSiswaActivity.this, AddSiswaActivity.class);
                startActivityForResult(i, INTENT_TAMBAH);
            }
        });
    }

    private void getSiswa() {
        //cek koneksi internet
        if (Utils.isNetworkOn(this)) {
            getListSiswa();
        } else {
            Utils.longToast(this, "Tidak ada koneksi");
        }
    }

    //request retrofit
    private void getListSiswa() {
        Call<SiswaResponse> call = RetrofitClient.provideRetrofit().getSiswa();
        call.enqueue(new Callback<SiswaResponse>() {
            @Override
            public void onResponse(Call<SiswaResponse> call, Response<SiswaResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    //assign list dari api
                    listSiswa = response.body().getResult();

                    //cek apakah siswa lebih dari 0
                    if (listSiswa.size() > 0) {
                        //setlist ke adapter recycler
                        siswaAdapter.setArray(listSiswa);
                        rvSiswa.setAdapter(siswaAdapter);

                    } else {
                        Utils.longToast(getApplicationContext(), "Tidak ada data");
                    }

                } else {
                    Utils.longToast(getApplicationContext(), "Tidak ada data");
                }
            }

            @Override
            public void onFailure(Call<SiswaResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                t.printStackTrace();
                Utils.longToast(getApplicationContext(), t.getLocalizedMessage());
            }
        });
    }

    //fungsi yg dijalankan ketika return dari AddSiswaActivity/EditSiswaActivity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //getDataSiswa lagi
            getSiswa();
        }
    }

    private void onClickAdapter() {
        //recycler onClick
        siswaAdapter.onClick(new SiswaAdapter.onItemClickListener() {
            @Override
            public void onItemClickListener(int position, Siswa siswa) {
                Intent i = new Intent(ListSiswaActivity.this, AddSiswaActivity.class);
                startActivityForResult(i, INTENT_EDIT);
            }
        });

        //recycler onLongClick (fungsi untuk menghapus siswa)
        siswaAdapter.onLongClick(new SiswaAdapter.onItemLongClickListener() {
            @Override
            public void onItemLongClickListener(int position, Siswa siswa) {
                showAlertDialog(siswa.getNpm());
            }
        });
    }

    //show alert dialog konfirmasi hapus siswa
    private void showAlertDialog(final String npm) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Perhatian");
        alertDialog.setMessage("Apakah anda ingin menghapus data ini?");

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                deleteSiswa(npm);
            }
        });

        alertDialog.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();

    }

    private void deleteSiswa(String npm) {
        if (Utils.isNetworkOn(this)) {
            postDeleteSiswa(npm);
        } else {
            Utils.longToast(this, "Tidak ada koneksi");
        }
    }

    private void postDeleteSiswa(String npm) {
        Call<DeleteSiswaResponse> call = RetrofitClient.provideRetrofit().deleteSiswa(npm);
        call.enqueue(new Callback<DeleteSiswaResponse>() {
            @Override
            public void onResponse(Call<DeleteSiswaResponse> call, Response<DeleteSiswaResponse> response) {
                if (response.body() != null) {
                    DeleteSiswaResponse response1 = response.body();
                    if (response1.getStatus().equalsIgnoreCase("OK")) {

                        Utils.longToast(getApplicationContext(), response1.getMessage());
                        getListSiswa();

                    } else {
                        Utils.longToast(getApplicationContext(), response1.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteSiswaResponse> call, Throwable t) {

                t.printStackTrace();
                Utils.longToast(getApplicationContext(), t.getLocalizedMessage());
            }
        });
    }
}