package com.tiketux.sample.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.tiketux.sample.R;
import com.tiketux.sample.model.Siswa;
import com.tiketux.sample.network.RetrofitClient;
import com.tiketux.sample.response.EditSiswaResponse;
import com.tiketux.sample.util.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mochamad Taufik on 18-Jun-19.
 * Email   : thidayat13@gmail.com
 */

public class EditSiswaActivity extends AppCompatActivity {

    private EditText etNama, etJurusan, etNoTelp, etAlamat, etKodePos;
    private Button btnSimpan;

    private ProgressDialog progressDialog;
    private Siswa siswa;

    private String npm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_siswa);

        etNama          = findViewById(R.id.et_nama);
        etJurusan       = findViewById(R.id.et_jurusan);
        etNoTelp        = findViewById(R.id.et_notelp);
        etAlamat        = findViewById(R.id.et_alamat);
        etKodePos       = findViewById(R.id.et_kodepos);
        btnSimpan       = findViewById(R.id.btn_simpan);

        siswa           = new Siswa();
        
        getBundle();

        progressDialog = new ProgressDialog(this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validasiForm();
            }
        });
    }

    private void getBundle() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            siswa   = extras.getParcelable("siswa");

            setToEditText();
        }
    }

    private void setToEditText() {
        npm     = siswa.getNpm();

        etNama.setText(siswa.getNama());
        etJurusan.setText(siswa.getJurusan());
        etNoTelp.setText(siswa.getNo_telp());
        etAlamat.setText(siswa.getAlamat());
        etKodePos.setText(siswa.getKodepos());
    }

    private void validasiForm() {
        if(npm.isEmpty() || etNama.getText().toString().isEmpty()
                || etJurusan.getText().toString().isEmpty() || etNoTelp.getText().toString().isEmpty()
                || etAlamat.getText().toString().isEmpty() || etKodePos.getText().toString().isEmpty()){
            Utils.longToast(this, getString(R.string.no_data));

        } else {
            if(Utils.isNetworkOn(this)){
                postForm();
            } else {
                Utils.longToast(this,getString(R.string.no_connection));
            }
        }
    }

    private void postForm() {

        progressDialog.setMessage("Load Data ...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Call<EditSiswaResponse> call = RetrofitClient.provideRetrofit().editSiswa(
                npm,
                etNama.getText().toString(),
                etJurusan.getText().toString(),
                etNoTelp.getText().toString(),
                etAlamat.getText().toString(),
                etKodePos.getText().toString());

        call.enqueue(new Callback<EditSiswaResponse>() {
            @Override
            public void onResponse(Call<EditSiswaResponse> call, Response<EditSiswaResponse> response) {
                progressDialog.dismiss();

                if (response.body() != null) {
                    EditSiswaResponse response1 = response.body();
                    if(response1.getStatus().equalsIgnoreCase("OK")){

                        Utils.longToast(getApplicationContext(), response1.getMessage());
                        Intent returnIntent =  new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();

                    }else{
                        Utils.longToast(getApplicationContext(), response1.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<EditSiswaResponse> call, Throwable t) {
                progressDialog.dismiss();
                Utils.longToast(getApplicationContext(), t.getLocalizedMessage());
            }
        });
    }
}
