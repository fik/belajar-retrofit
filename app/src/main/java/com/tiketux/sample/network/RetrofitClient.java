package com.tiketux.sample.network;

import com.tiketux.sample.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mochamad Taufik on 13-Jun-19.
 * Email   : thidayat13@gmail.com
 */

//class untuk inisialisasi retrofit
public class RetrofitClient {

    private static Retrofit retrofit = null;

    //base url yang dituju sesuaikan dengan ip komputer, karena ini masih localhost pastikan android & komputernya 1 koneksi
    private static String BASE_URL = "http://192.168.4.146/siswa/api/";

    //inisialisasi retrofit
    public static Retrofit getClient() {
        retrofit = null;
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //parse menggunakan gson
                .client(okHttpClient()) //set client okhttp
                .build();

        return retrofit;
    }

    private static OkHttpClient okHttpClient() {
        //http logging untuk menampilkan log url dan response dari retrofit untuk dimunculkan di logcat
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if(BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }else{
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        return new OkHttpClient.Builder()
                .addNetworkInterceptor(interceptor) //set logging
                .readTimeout(60, TimeUnit.SECONDS) //read timeout jika req lebih dari 60 detik maka di cancel
                .connectTimeout(60, TimeUnit.SECONDS) //connect timeout jika req lebih dari 60 detik maka di cancel
                .build();
    }

    //fungsi yang dipanggil di activity
    public static RetrofitInterface provideRetrofit(){
        return RetrofitClient.getClient().create(RetrofitInterface.class);
    }
}
