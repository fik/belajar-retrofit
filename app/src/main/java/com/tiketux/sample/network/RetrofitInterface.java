package com.tiketux.sample.network;

import com.tiketux.sample.response.DeleteSiswaResponse;
import com.tiketux.sample.response.EditSiswaResponse;
import com.tiketux.sample.response.SiswaResponse;
import com.tiketux.sample.response.TambahSiswaResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Mochamad Taufik on 13-Jun-19.
 * Email   : thidayat13@gmail.com
 */

//interface retrofit, untuk define url endpoint yg di tuju, parameter yg dikirim, metode dan header
//metode yang paling sering digunakan yaitu @GET, @POST dan @PUT

public interface RetrofitInterface {

    @GET("siswa.php")
    Call<SiswaResponse> getSiswa();

    @FormUrlEncoded
    @POST("input_data.php")
    Call<TambahSiswaResponse> inputSiswa(
            @Field("npm") String npm,
            @Field("nama") String nama,
            @Field("jurusan") String jurusan,
            @Field("notelp") String notelp,
            @Field("alamat") String alamat,
            @Field("kodepos") String kodepos
    );

    @FormUrlEncoded
    @POST("edit.php")
    Call<EditSiswaResponse> editSiswa(
            @Field("npm") String npm,
            @Field("nama") String nama,
            @Field("jurusan") String jurusan,
            @Field("notelp") String notelp,
            @Field("alamat") String alamat,
            @Field("kodepos") String kodepos
    );

    @FormUrlEncoded
    @POST("delete.php")
    Call<DeleteSiswaResponse> deleteSiswa(
            @Field("npm") String npm
    );
}
