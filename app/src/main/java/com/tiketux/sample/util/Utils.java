package com.tiketux.sample.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;


/**
 * Created by Mochamad Taufik on 04-Dec-18.
 * Email   : thidayat13@gmail.com
 */

public class Utils {

    //fungsi untuk mengecek koneksi, jgn lupa menambahkan permission di manifest juga
    public static boolean isNetworkOn(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService
                (Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());
    }

    //untuk set Toolbar
    public static void setToolbar(Toolbar toolbar, String title, AppCompatActivity activity){
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setTitle(title);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    public static void setToolbar(Toolbar toolbar, AppCompatActivity activity){
        activity.setSupportActionBar(toolbar);
    }

    //untuk set Toast
    public static void longToast(Context context, String text){
        Toast.makeText(context,text, Toast.LENGTH_LONG).show();
    }

    public static void longToast(Context context, CharSequence text){
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static void shortToast(Context context, String text){
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    public static void shortToast(Context context, CharSequence text){
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    //untuk set Snackbar
    public static void longSnackbar(View view, String text){
        Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
    }

    public static void shortSnackbar(View view, String text){
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show();
    }

    public static void indefinitifSnackbar(View view, String text){
        Snackbar.make(view, text, Snackbar.LENGTH_INDEFINITE).show();
    }
}
