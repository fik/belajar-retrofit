package com.tiketux.sample.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by fuadhamidan on 30 Jul 2015.
 * Email    : fuadhamidan@gmail.com
 * Twitter  : @fufufufuad
 * --
 * Tiketux
 * com.trust.android.tiketux.views.widgets.DividerItemDecorationPadding
 * -Desc class
 */

//divider untuk recycler view
public class DividerItemDecorationPadding extends RecyclerView.ItemDecoration {
    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

    private Drawable mDivider;

    public DividerItemDecorationPadding(Context context) {
        final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);

        mDivider = styledAttributes.getDrawable(0);

        styledAttributes.recycle();
    }

    public DividerItemDecorationPadding(Context context, int resId) {
        mDivider = ContextCompat.getDrawable(context, resId);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left    = parent.getPaddingLeft();
        int right   = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();

        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    /*@Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left    = 16;
        outRect.right   = 16;
    }*/
}
