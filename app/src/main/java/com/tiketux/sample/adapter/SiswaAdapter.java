package com.tiketux.sample.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tiketux.sample.R;
import com.tiketux.sample.model.Siswa;

import java.util.List;

/**
 * Created by Mochamad Taufik on 13-Jun-19.
 * Email   : thidayat13@gmail.com
 */

//adapter untuk menampung data list
public class SiswaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Siswa> arrlist;

    public TextView tvNama, tvJurusan, tvAlamat, tvTelepon;

    private static Context context;

    //deklarasi listener
    private onItemClickListener mListener;
    private onItemLongClickListener mListenerLong;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext()).inflate(R.layout.row_siswa, parent, false);
        return new RecyclerView.ViewHolder(view) {
        };
    }

    //setArray
    public void setArray(List<Siswa> arrlist) {
        this.arrlist = arrlist;
    }

    public SiswaAdapter(Context c) {
        context = c;
    }

    //setListener agar bisa di panggil di activity
    public void onClick(onItemClickListener listener) {
        mListener = listener;
    }

    //setListener agar bisa di panggil di activity
    public void onLongClick(onItemLongClickListener listener) {
        mListenerLong = listener;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        tvNama              = holder.itemView.findViewById(R.id.nama);
        tvJurusan           = holder.itemView.findViewById(R.id.jurusan);
        tvAlamat            = holder.itemView.findViewById(R.id.alamat);
        tvTelepon           = holder.itemView.findViewById(R.id.notelp);

        //set data ke textview
        tvNama.setText(arrlist.get(position).getNama());
        tvJurusan.setText(arrlist.get(position).getJurusan());
        tvAlamat.setText(arrlist.get(position).getAlamat());
        tvTelepon.setText(arrlist.get(position).getNo_telp());

        //onClick
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //menggunakan listener agar onclick dapat dipanggil di activity
                if (mListener != null)
                    mListener.onItemClickListener(position, arrlist.get(position));
            }
        });

        //onLongClick
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //menggunakan listener agar onclick dapat dipanggil di activity
                if (mListenerLong != null)
                    mListenerLong.onItemLongClickListener(position, arrlist.get(position));

                return true;
            }
        });

    }

    //interface listener onClick
    public interface onItemClickListener {
        public void onItemClickListener(int position, Siswa siswa);
    }

    //interface listener onLongClick
    public interface onItemLongClickListener {
        public void onItemLongClickListener(int position, Siswa siswa);
    }

    @Override
    public int getItemCount() {
        return arrlist.size();
    }
}

