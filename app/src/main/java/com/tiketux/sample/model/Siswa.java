package com.tiketux.sample.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mochamad Taufik on 13-Jun-19.
 * Email   : thidayat13@gmail.com
 */

// Model berfungsi untuk menampung data sementara
// Parcelable berfungsi agar data model dapat dipindahkan ke activity lain menggunakan (i.putExtra)
public class Siswa implements Parcelable {


    private String npm;
    private String nama;
    private String jurusan;
    private String no_telp;
    private String alamat;
    private String kodepos;

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKodepos() {
        return kodepos;
    }

    public void setKodepos(String kodepos) {
        this.kodepos = kodepos;
    }

    // di generate menggunakan plugin Android Parcelable code generator
    // kalau sudah di install plugin nya alt+insert -> parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.npm);
        dest.writeString(this.nama);
        dest.writeString(this.jurusan);
        dest.writeString(this.no_telp);
        dest.writeString(this.alamat);
        dest.writeString(this.kodepos);
    }

    public Siswa() {
    }

    protected Siswa(Parcel in) {
        this.npm = in.readString();
        this.nama = in.readString();
        this.jurusan = in.readString();
        this.no_telp = in.readString();
        this.alamat = in.readString();
        this.kodepos = in.readString();
    }

    public static final Parcelable.Creator<Siswa> CREATOR = new Parcelable.Creator<Siswa>() {
        @Override
        public Siswa createFromParcel(Parcel source) {
            return new Siswa(source);
        }

        @Override
        public Siswa[] newArray(int size) {
            return new Siswa[size];
        }
    };
}
