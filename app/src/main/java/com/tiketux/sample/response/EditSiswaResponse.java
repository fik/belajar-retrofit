package com.tiketux.sample.response;

/**
 * Created by Mochamad Taufik on 18-Jun-19.
 * Email   : thidayat13@gmail.com
 */

public class EditSiswaResponse {

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
