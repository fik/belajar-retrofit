package com.tiketux.sample.response;

import com.tiketux.sample.model.Siswa;

import java.util.List;

/**
 * Created by Mochamad Taufik on 13-Jun-19.
 * Email   : thidayat13@gmail.com
 */

//response dari api
public class SiswaResponse {

    private String status;
    private String message;
    private List<Siswa> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Siswa> getResult() {
        return result;
    }

    public void setResult(List<Siswa> result) {
        this.result = result;
    }

}
